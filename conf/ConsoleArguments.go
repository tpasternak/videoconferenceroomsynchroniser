package conf

import (
	"flag"
)

var argumentPointers = make(map[string]*string)
var arguments = make(map[string]string)

func init() {
	getConsoleArguments()
}

func getConsoleArguments() {

	defineConfigFlags()
	flag.Parse()

	//We have to convert the pointer to a datatype. Here we're doing it :)
	for key := range argumentPointers {
		// *<var> dereferences a pointer
		arguments[key] = *argumentPointers[key]
	}
}

func defineConfigFlags() {
	argumentPointers["configPath"] = flag.String("config", "config.yml", "Path to config file (Default: config.yml")
}
